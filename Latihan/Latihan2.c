#include <stdio.h>

int main(){
         printf("Nama: Richwen Canady\n");
         int umur = 18;
         float berat = 58.3;
         double tinggi = 165;
         char jenisKelamin = 'L';

         printf("Umur: %i tahun\n", umur);
         printf("Berat: %.2f cm\n", berat);
         printf("Tinggi: %.2f Kg\n", tinggi);
         printf("Jenis Kelamin: %c\n", jenisKelamin);

         return 0;

}
